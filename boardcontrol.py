#This file was created by Yombo for use with Yombo Gateway automation
#software.  Details can be found at https://yombo.net
"""
This file is apart of the piface digital module. Its' responsible for polling
board input status. This is required since the native pifacecommon uses multitasking,
which is incompatible with twisted. This uses threads and moves the workload out
of the twisted reactor.

This module is responsible constantly polling board status and sending it
to the piface_digital module.

License
=======

This module is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The **`Yombo.net <http://www.yombo.net/>`_** team and other contributors
hopes that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

The GNU General Public License can be found here: `GNU.Org <http://www.gnu.org/licenses>`_

.. moduleauthor:: Mitch Schwenk <mitch-gw@yombo.net>
:copyright: Copyright 2017 by Yombo.
:license: GPL(v3)
:organization: `Yombo <https://yombo.net>`_
"""
try:
    import pifacedigitalio
    HAS_PIDIGITAL = True
except ImportError:
    HAS_PIDIGITAL = False

from time import time, sleep

from twisted.internet import reactor

from yombo.core.exceptions import YomboWarning

class BoardControl(object):
    def __init__(self, input_change_callback, last_status):
        """
        Iterates through all the possible boards to build an inventory. Sets the
        starting values to a previous values saved.

        :param input_change_callback: A callback to call when status changes.
        :param last_status: A dictionary, the results of 'get_status()' below.
        """
        self.do_loop = True

        self.input_change_callback = input_change_callback
        self.boards = {}
        for i in range(7):  # currently, only 4 boards are supported, but we just catch the error the same...
            try:
                board = pifacedigitalio.PiFaceDigital(hardware_addr=i, init_board=False)
                self.boards[i] = {
                    'board': board,
                    'status': {
                        0: None,
                        1: None,
                        2: None,
                        3: None,
                        4: None,
                        5: None,
                        6: None,
                        7: None,
                    }
                }
                if i in last_status:
                    self.boards[i]['status'] = last_status[i]

            except pifacedigitalio.NoPiFaceDigitalDetectedError as e:
                pass
        self.start()

    def run(self):
        """
        Runs the polling. Calls the input_changed_callback when something changes.

        :return:
        """
        while self.do_loop:
            sleep(0.02)
            for board_id, board in self.boards.items():
                all_input_status = '{0:08b}'.format(board['board'].input_port.value)[::-1]
                input_id = 0
                for status in all_input_status:
                    new_status = int(status)

                    if board['status'][input_id] != new_status:
                        board['status'][input_id] = new_status
                        reactor.callFromThread(self.input_change_callback, board_id, input_id, new_status, time())
                    input_id += 1

    def turn_on(self, board_id, output_id):
        """
        Turn on an output.

        :param board_id:
        :param output_id:
        :return:
        """
        if board_id not in self.boards:
            raise YomboWarning("Board Control inside PiFace Digital doesn't have a board id: %s" % board_id)
        self.boards[board_id]['board'].output_pins[output_id].turn_on()

    def turn_off(self, board_id, output_id):
        """
        Turn off an output.
        :param board_id:
        :param output_id:
        :return:
        """
        if board_id not in self.boards:
            raise YomboWarning("Board Control inside PiFace Digital doesn't have a board id: %s" % board_id)
        self.boards[board_id]['board'].output_pins[output_id].turn_off()

    def turn_toggle(self, board_id, output_id):
        """
        Toggle an output.

        :param board_id:
        :param output_id:
        :return:
        """
        if board_id not in self.boards:
            raise YomboWarning("Board Control inside PiFace Digital doesn't have a board id: %s" % board_id)
        self.boards[board_id]['board'].output_pins[output_id].turn_toggle()

    def start(self):
        """
        Start polling.

        :return:
        """
        self.do_loop = True
        reactor.callInThread(self.run)

    def stop(self):
        """
        Stop polling.
        :return:
        """
        self.do_loop = False

    def get_status(self):
        """
        Returns a dictionary of status. Can be used to send back into __init__ to set the status.
        :return:
        """
        status = {}
        for board_id, device in self.boards.items():
            status[board_id] = device['status']
        return status

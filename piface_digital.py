#This file was created by Yombo for use with Yombo Gateway automation
#software.  Details can be found at https://yombo.net
"""
PiFace Digital
==============

The PiFace Digital board allows easy access to control outputs and get
status of inputs.

Implements an interface between Jabber and YomboBot module. Allows
interactions between the YomboBot module through a Jabber chat
session.

.. warning::

   You must be using a Raspberry Pi with a PiFace Digital 1 or 2 connected. For
   suggested parts list see:
   https://yombo.net/docs/gateway/install/raspberry_pi_bare_metal/

.. warning::

   You must enable the SPI setting on the Raspberry Pi. See:
   https://yombo.net/docs/kb/enable_spi_piface_digital_boards/


License
=======

This module is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The **`Yombo.net <http://www.yombo.net/>`_** team and other contributors
hopes that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

The GNU General Public License can be found here: `GNU.Org <http://www.gnu.org/licenses>`_

.. moduleauthor:: Mitch Schwenk <mitch-gw@yombo.net>
:copyright: Copyright 2017 by Yombo.
:license: GPL(v3)
:organization: `Yombo <https://yombo.net>`_
"""
try:
    import pifacedigitalio
    HAS_PIDIGITAL = True
except ImportError:
    HAS_PIDIGITAL = False

from twisted.internet.task import LoopingCall
from twisted.internet.defer import inlineCallbacks

from yombo.core.log import get_logger
from yombo.core.module import YomboModule

from .boardcontrol import BoardControl

logger = get_logger('modules.PiFace_Digital')

class PiFace_Digital(YomboModule):
    """
    PiFace Digital Module
    """
    def _init_(self, **kwargs):
        """
        Setup basic module attributes.
        """
        if HAS_PIDIGITAL is False:
            logger.warn("PiFace_Digital is disabled, python module not detected. Install with 'sudo apt-get install python-pifacedigitalio'.")
            return

        self.input_ports = {}
        self.input_devices = {} # used to lookup house/unit to a device
        self.output_devices = {} # used to lookup house/unit to a device

        self.update_status_loop = LoopingCall(self.update_status)

        self.cmd_map = {
            'open': 0,
            'closed': 1,
        }

    @inlineCallbacks
    def _load_(self, **kwargs):
        """
        Load the list of devices we manage/monitor. Then, start monitoring devices.
        """
        if HAS_PIDIGITAL is False:
            return

        for device_id, device in self._ModuleDevices().items():
            try:
                port = int(device.device_variables['port']['data'][0]['value'])
                board = int(device.device_variables['board']['data'][0]['value'])
            except:
                logger.warn("Device {device} does not have required variable 'port'", device=device.label)
                continue

            try:
                deviceType = self._DeviceTypes[device.device_type_id]
            except:
                logger.warn("Device {device} does not appear to have a valid device type id: {device_type_id}", device=device.label, device_type_id=device.device_type_id)
                continue

            if deviceType.machine_label == 'piface_digital_input':
                self.input_devices[device.device_id] = {
                    'device': device,
                    'port': port,
                    'board': board,
                }
                input_id = "%s:%s" % (board, port)
                self.input_ports[input_id] = device.device_id

            elif deviceType.machine_label == 'piface_digital_output':
                self.output_devices[device.device_id] = {
                    'device': device,
                    'port': port,
                    'board': board,
                }

        self.last_status = yield self._SQLDict.get(self, "last_status")  # 'self' is required for data isolation
        self.pifacei\
            = BoardControl(self.input_changed, self.last_status)

        self.update_status_loop.start(300, False)

    def _stop_(self, **kwargs):
        """
        Brings the monitoring to the halt, saves the current status.
        :return:
        """
        if HAS_PIDIGITAL is False:
            return

        if self.update_status_loop is not None and self.update_status_loop.running:
            self.update_status_loop.stop()
        self.piface.stop()
        self.update_status()

    def update_status(self):
        """
        We track the status of everything so that on next startup, only report what has changed to the
        system.

        :return:
        """
        self.last_status.update(self.piface.get_status())


    def input_changed(self, board_id, input_id, status, change_time):
        """
        This is called by the boardcontrol module when an input has changed.

        :param board_id: int - board number
        :param input_id: int - port number
        :param status: int - 0 for open (low), 1 for closed (high)
        :param change_time: timestamp of the change.
        :return:
        """
        logger.debug("Input changed - board({board_id}), input({input_id}), status({status}), time({change_time})",
            board_id=board_id, input_id=input_id, status=status, change_time=change_time)

        address = "%s:%s" % (board_id, input_id)

        if address not in self.input_ports:
            logger.warn("Received an input port status change, but port seems to be missing: Board: {board}  Port: {port}",
                board=board_id, port=input_id)
            return

        machine_status=None
        human_status = None

        if status == 1:
            machine_status = 'close'
            human_status = 'Closed'
        elif status == 0:
            machine_status = 'open'
            human_status = 'Opened'
        else:
            logger.warn("Input port ({port}) shows invalid status: {status}", port=address, status=status)

        logger.debug("status update.  Machine: {machine_status}   Human: {human_status}", machine_status=machine_status,
                    human_status=human_status)

        deviceObj=self.input_devices[self.input_ports[address]]['device']
        deviceObj.set_status(machine_status=machine_status, human_status=human_status, source="Module:PiFace_Digital")

    def _device_command_(self, **kwargs):
        """
        Received a request to do perform a command for a piface digital output interface.

        :param kwags: Contains 'device' and 'command'.
        :return: None
        """
        if HAS_PIDIGITAL is False:
            return

        # logger.error("PiFace_Digital received device_command:")

        device = kwargs['device']

        if self._is_my_device(kwargs['device']) is False:
            logger.warn("This is not a device for me.")
            return

        request_id = kwargs['request_id']
        command = kwargs['command']

        device.device_command_received(request_id, message=_('module.pifacedigital', 'Handled by PiFace_Digital module.'))

        if device.device_id in self.output_devices:
            logger.debug("Handling output port request: %s" % command.machine_label)
            board_id = self.output_devices[device.device_id]['board']
            port_id = self.output_devices[device.device_id]['port']

            if command.machine_label == "open":
                self.piface.turn_off(board_id, port_id)
            elif command.machine_label == "close":
                self.piface.turn_on(board_id, port_id)
            else:
                logger.warn("PiFace Digital recieved an invalid command: %s", command.machine_label)
                device.device_command_failed(request_id, message=_('module.pifacedigital',
                                                                   'PiFace_Digital could not process output port command: %s' % command.machine_label))
                return

            device.device_command_done(request_id,
                                       message=_('module.pifacedigital', 'Command completed by PiFace_Digital module.'))

            device_status = {
                'human_status': command.label,
                'machine_status': command.machine_label,
                'request_id': request_id,
                'source': 'Module:PiFace_Digital',
            }

            device.set_status(**device_status)  # set and send the status of the piface digital output port
            device.device_command_done(request_id, message=_('module.pifacedigital',
                                                               'PiFace_Digital completed command.'))

        elif device.device_id in self.input_devices:
            logger.warn("Handling input port request.  Nothing to actually do.")
            return



